const app=require('../app')
const request = require("supertest");

describe("App test", () => {
    it("has a module", () => {
      expect(app).toBeDefined();
    });
  
    let server;
  
    beforeAll(() => {
      server = app.listen(3001);
    });
  
    afterAll(done => {
      server.close(done);
    });
  
    describe("user route test", () => {
      it("Has hit user", async () => {
        await request(server)
          .post("/user")
          .expect(200);
      });

    });
});