#!/bin/bash
dir=/home/sushil/dockerpractice

#!/bin/bash

repos=( 
  "/home/sushil/dockerpractice"
)

echo ""
echo "Getting latest for" ${#repos[@]} "repositories using pull --rebase"

for repo in "${repos[@]}"
do
  echo ""
  echo "****** Getting latest for" ${repo} "******"
  cd "${repo}"
  git add .
  git commit -m "Staging changes"
  git pull --rebase
  echo "******************************************"
done
sudo docker-compose -f "$dir/docker-compose.yml" up --build -d
echo
echo "DONE"